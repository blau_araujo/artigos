# Propriedade Intelectual - Crime de Lesa Humanidade

> Tema apresentado na Live de Segunda #19 (Mar 02, 2020)

## A Ilusão da Propriedade Intelectual

A partir dos anos '90, o uso da expressão **propriedade intelectual** começou a ser generalizado. Não era uma expressão nova, pois havia sido cunhada nos anos '60 pelas Nações Unidas quando fundou a **Organização Mundial de Propriedade Intelectual** (OMPI), em 1967.

Embora seja uma organização das Nações Unidas, a OMPI foi criada para representar os interesses das corporações e entidades detentoras de três conceitos jurídicos diferentes e sem relação direta entre si: *copyright*, *marcas* e *patentes*, provocando propositalmente certas distorções que beneficiam apenas essas mesmas corporações.

Entre outras, porém, a distorção mais relevante é a associação entre *copyright*, marcas e patentes com algum tipo de direto de propriedade sobre objetos e bens materiais, tangíveis e escassos. Consequentemente, mesmo sabendo perfeitamente que esse tipo de associação vai contra o espírito das leis específicas de cada um dos três temas, as corporações têm forçado sistematicamente os legisladores a modificarem essas leis a fim de aproximarem cada vez mais a analogia entre *"propriedades intelectuais"* e o direito sobre coisas físicas.

## A ideia por detrás dessas três legislações

Para entender melhor o assunto, vejamos o que trata, em essência, cada uma dessas três legislações.

### Leis de Copyright e Direitos Autorais

Originalmente pensadas para proteger e incentivar a autoria e as artes, as leis de direitos autorais, como o nome diz, deveriam contemplar os autores, garantindo que um conjunto bem detalhado de características da sua obra fossem atribuídos diretamente a ele, o único legal e moralmente atribuído do direito de decidir o que fazer com seu trabalho.

Contudo, uma das atribuições garantidas ao autor, entre várias, é a decisão sobre as cópias de suas obras. Num primeiro momento, você pode imaginar que esse aspecto trata de livros e arquivos de música, por exemplo, mas não era esse o espírito original desse tipo de legislação. Isso foi acrescentado com o tempo e, em momento algum, visou-se proteger o autor e sua obra. O princípio em questão era que a autoria em si fosse reconhecida. Por exemplo, no caso em que eu componho uma música e, mais tarde, vem outra pessoa, pega vários trechos da minha música e diz que aquela composição é de autoria dela!

Era esse tipo de questão que abordavam as leis de direitos autorais. Mas, com o tempo, um outro tipo de lei, bem como um conjunto totalmente diferente de princípios, foram sendo inseridos sutilmente no conceito de "direitos autorais", ao ponto de, hoje, ser completamente irrelevante quem é o autor. No dias de hoje, o que importa são os **direitos de cópia**, ou seja, o ***copyright***.

### Leis de patentes

Aqui a coisa é ainda mais confusa, pois essas leis pretendem contemplar os direitos de autoria sobre ideias, nas áreas mais variadas e absurdas, por mais abstratas que sejam, desde que tenham sido devidamente registradas. Na essência, as patentes são concessões públicas do Estado para que o titular (não necessariamente o "autor") tenha a exclusividade de explorar comercialmente suas ideias por um tempo limitado. Obviamente, trata-se de uma propriedade **industrial**, algo que passa longe da ideia de propriedade **intelectual**.

Um aspecto interessante das leis de patentes, é que a exclusividade refere-se ao direito de prevenção de que outros fabriquem, comercializem e utilizem o objeto da concessão. Por outro lado, ao conceder a concessão, a maioria das legislações exige que o conhecimento dos pontos essenciais que caracterizam a novidade da patente devam ser disponibilizados ao público. Isso, e a expiração dos termos da concessão, existem para garantir que o progresso não seja estagnado, ou seja, para permitir que outros desenvolvimentos surjam a partir de partes das ideias anteriores e, ao final do contrato, aquela ideia torne-se conhecimento.

Também é garantido o reconhecimento do autor da ideia, mas de uma forma bem distante das questões abordadas pelas leis de direitos autorais, já que, em essência, as patentes existem para incentivar a busca pelo conhecimento, a eficiência econômica, a pesquisa e o desenvolvimento. Mas, não preciso dizer o quanto esse princípio foi distorcido, pois todos sabem (mesmo que não queiram admitir) que, numa disputa entre interesses comerciais de mega corporações e interesses sociais e públicos, quem sempre sairá ganhando.

Felizmente, as leis brasileiras não reconhecem patentes para:

  * Descobertas, teorias científicas e métodos matemáticos;
  * Concepções puramente abstratas;
  * Esquemas, planos, princípios ou métodos comerciais, contábeis, financeiros, educativos, publicitários, de sorteio e de fiscalização;
  * Obras literárias, arquitetônicas, artísticas e científicas ou qualquer criação estética;
  * Programas de computador em si;
  * Apresentação de informações;
  * Regras de jogos;
  * Técnicas e métodos operatórios, bem como métodos terapêuticos ou de diagnóstico, para aplicação no corpo humano ou animal;
  * Todo ou parte de seres vivos naturais e materiais biológicos encontrados na natureza, ou ainda que dela isolados, inclusive o genoma ou germoplasma de qualquer ser vivo natural e os processos biológicos naturais.

### Leis de marcas

Dessas três, entre as várias leis jogadas no "balaio" da propriedade intelectual, é a que menos tem a ver com tudo que dissemos até aqui! Marcas são identificações, são uma forma de fazer o consumidor saber de quem está comprando alguma coisa.

Mas, as legislações foram tão distorcidas que, em vez de proteger todos os elementos componentes de uma marca, algumas corporações tentam (nem sempre com sucesso, felizmente) ampliar seus direitos para além dos limites da identificação comercial, entrando no campo da censura (quando impedem um consumidor de reclamar citando alguma marca), da exploração audiovisual (quando um filmes, e até vídeos no Youtube, podem ser penalizados por exibir alguma marca, mesmo que ao fundo e sem qualquer relação com a produção exibida) ou simplesmente tentando impedir que palavras comuns ao idioma sejam utilizadas na composição de outras marcas.

## Propriedade Intelectual Não Existe!

Tendo visto o problema das leis de copyright (e direitos autorais), marcas e patentes, e como elas são diferentes o suficiente para serem tratadas cada uma no escopo dos seus princípios e métodos específicos, fica muito clara a distorção que é justificar interesses e alegações com base na ideia e nos valores de uma suposta *"propriedade intelectual"*. Na verdade, a grande maioria das afirmações que trazem o argumento da propriedade intelectual à baila são falsos!

O fato é que a própria expressão *"propriedade intelectual"* não faz nenhum sentido e só serve para atribuir valores morais ao que são meramente interesses comerciais de controle e exploração. A própria OMPI, a partir de 1974, foi estabelecida como uma agência especializada da ONU com o propósito de harmonizar os interesses comerciais com o interesse público e as metas humanitárias da ONU. Por isso, a proposta da OMPI foi redefinida para: *"a promoção da atividade intelectual criativa e a facilitação da transferência de tecnologia relacionada à propriedade industrial para os países em desenvolvimento de forma a acelerar seu desenvolvimento econômico, social e cultural"*.

Então, pergunte-se: como a última música do seu cantor *pop* favorito pode facilitar a transferência de tecnologia da propriedade industrial para países em desenvolvimento? Isso talvez se aplique a patentes, mas muito raramente, para não dizer jamais, a temas relacionados com marcas, direitos autorais e copyright. Mas, tudo bem! Você não pode ouvir, copiar ou compartilhar um mp3 do *hit* do momento porque... "Propriedade intelectual! Você está impedindo que as populações carentes da Somália saiam da miséria! Você é um criminoso, um pirata!"

Aliás, quando o batalhão de advogados bater à sua porta, ninguém vai querer saber que você não tirou nada de ninguém, não interferiu em nada no mercado (a não ser colaborando com a propagação daquele "sucesso"), não explorou comercialmente nada, não disse que a composição era sua e nem que você era a pessoa que estava cantando naquela gravação.

Da mesma forma, ninguém vai mencionar o fato de que basta ligar um rádio ou uma TV aberta para ouvir milhares de vezes aquela música. Só que, neste caso, existe um detalhe: junto com a música, você é bombardeado com propagandas de todos os tipos, inclusive as que dizem como você deve se comportar, em quem votar, e o que deve considerar certo ou errado.

A propriedade intelectual é algo que não existe, mas a ideia que a expressão evoca é utilizada quase sempre como uma ferramenta da controle. A propósito, Richard Stallman costuma chamar essa distorção criada pela ideia de propriedade intelectual de "colonização legislativa", que é quando, entre outras medidas, países poderosos tentam impor legislações injustas, entre elas as leis sobre as supostas propriedades intelectuais, para países em posição de inferioridade.

## Crime de Lesa Humanidade

Tal como proposta e aplicada na prática, a noção de propriedade intelectual permite a distorção de legislações específicas não-relacionadas sob o falso argumento de que ideias, invenções, criações artísticas e literárias, bem como outras manifestações inatas da humanidade, são bens escassos e alienáveis e, portanto, sujeitos aos mesmos tipos de leis que regem o direito à propriedade.

A verdade é que o desenvolvimento humano deveria estar acima da proteção de patentes e direitos de cópia. Dificilmente surge o pedido de uma patente que não seja o resultado de outras patentes, porque é assim que funciona o desenvolvimento do conhecimento humano. Mas o que prevalece é o abuso e a exploração dos verdadeiros detentores desses direitos, que raramente são os autores, e sim corporações que nada criam ou produzem além de entraves ao livre fluxo de técnicas e ideias, sempre colocando o interesses dos acionistas acima de tudo.

A criatividade humana, sua engenhosidade, a sua ciência e a sua cultura, são frutos da ação colaborativa síncrona e assíncrona de grupos e pessoas desde a nossa origem. Foi apenas no último século que decidimos que estava na hora de negar a nossa natureza e impedir o livre acesso e uso de ideias. Foi apenas nos últimos cem anos que escolhemos tomar posse do fruto do nosso intelecto com a finalidade única do lucro egoísta, a despeito da miséria e do atraso no restante do mundo.

Se dizem que o Século 20 foi marcado pelas grandes descobertas científicas e pelos avanços tecnológicos, não podemos negar que é quase impossível saber se ainda estaríamos à beira da insustentabilidade, correndo riscos reais como espécie, se estes mesmos saberes e técnicas estivessem abertos para quem quisesse elaborar soluções e, quem sabe, evitar o que estamos vivendo hoje.

Sim, não é possível saber com certeza. Mas sabemos com certeza que precisamos rever a nossa relação com a produção intelectual. Precisamos, com urgência, de mais cabeças pensando e criando, precisamos da liberdade de acesso às pesquisas científicas, de mais obras produzidas coletivamente, pela cooperação de muitos em nome de toda a humanidade. Precisamos que músicos, escritores e artistas retomem o poder sobre suas obras, eles não precisam mais das gravadoras e de outras corporações especializadas em publicar, mas também em decidir o que será visto, lido ou ouvido.

Enquanto isso não acontecer, a suposta propriedade intelectual continuará tirando de nós qualquer possibilidade de desenvolvimento numa direção capaz de proporcionar uma vida melhor e mais sustentável para todos, e isso é um verdadeiro crime contra a humanidade.
