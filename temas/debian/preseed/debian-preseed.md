# Customizando a instalação do Debian com preseed

[[_TOC_]]

## 1 - O que é preseeding

* O preseeding é um método de automação e padronização de instalações do Debian.
* Através de um arquivo de configuração, nós podemos predefinir as respostas para várias perguntas feitas no processo de instalação.
* É possível acessar o arquivo de configuração pela rede, internet ou em outro dispositivo no momento da instalação.
* Também é possível modificar a imagem de instalação para inserir o arquivo de configuração e fazer com que ele seja executado automaticamente.
* O preseeding oferece algumas funcionalidades não encontradas nas instalações normais, como a possibilidade de executar comandos antes e depois da instalação, definir a localização de outros repositórios e pacotes a serem instalados.

## 2 - Onde encontrar informações

* Guia de Instalação do Debian GNU/Linux: [Apêndice B - Automatizar a instalação utilizando 'preseeding'](https://www.debian.org/releases/stable/amd64/apb.pt.html)
* O Manual do Administrador Debian: [12.3. Instalação Automatizada](https://debian-handbook.info/browse/pt-BR/stable/sect.automated-installation.html)
* Debian Wiki: [Preseeding d-i](https://wiki.debian.org/DebianInstaller/Preseed)
* Debian Wiki: [Modifying an installation ISO image to preseed the installer from its initrd](https://wiki.debian.org/DebianInstaller/Preseed/EditIso)
* Debian Wiki: [Remastering a Debian installation ISO or a CD/DVD image](https://wiki.debian.org/DebianInstaller/Modify/CD)

## 3 - Métodos de preseeding

O preseeding é feito através de um arquivo de configuração do Debian Installer (`d-i`), geralmente chamado `preseed.cfg`. Para que a automação aconteça, o instalador precisa ser capaz de ler esse arquivo, o que pode ser feito a partir de três métodos:

* Lendo o arquivo em outro dispositivo (HDD, SSD, pendrive, etc) disponível no momento da instalação.
* Lendo o arquivo em algum local na rede ou na internet que possa ser acessado no momento da instalação.
* Alterando o `initrd` da imagem de instalação para inserir o arquivo de configuração.

O próprio instalador do Debian oferece a opção de carregar o arquivo de preseed no menu `Advanced Options > Automated Install`.

<img src="preseed-menu.png" witdh="400" height="300" />

Neste caso, assim que a rede é configurada, nós podemos informar a localização do arquivo, como na imagem abaixo:

<img src="preseed-screen.png" witdh="400" height="300" />

Ainda com o instalador, teclando `ESC` no menu, nós temos acesso às opções de `boot`, onde é possível informar a localização do arquivo de preseed, por exemplo:

```
boot: auto url=https://debxp.org/debian/preseed.cfg
```

<img src="preseed-boot.png" witdh="400" height="300" />

## 4 - Limitações

* A principal limitação do preseeding está na etapa de particionamento.
* É possível automatizar um esquema de particionamento, mas ele é aplicável apenas ao disco inteiro ou a um espaço livre não particionado.
* Não é possível utilizar partições existentes!

## 5 - O arquivo de configuração do preseed

* A etapa mais importante do preseeding é a criação de um arquivo de configuração.
* O arquivo pode ter qualquer nome, exceto quando ele for inserido na imagem de instalação, caso em que deverá ter obrigatoriamente o nome `preseed.cfg`.
* A razão disso é que, ao iniciar, o instalador verificará se existe um arquivo com o nome `preseed.cfg` no local certo para carregá-lo.
* A melhor forma de começar um arquivo de preseed é editando as opções de um arquivo de exemplo.

Onde encontrar informações sobre as opções do `preseed.cfg`:

* [Exemplo do preseed.cfg](https://www.debian.org/releases/buster/example-preseed.txt)
* [Descrição detalhada das opções](https://www.debian.org/releases/stable/amd64/apbs04.pt.html)

## 6 - Exemplos de opções

A seguir, veremos algumas opções do `preseed.cfg` que podem ser interessantes.

### Padronizar idioma, pais e layout de teclado

```
d-i debian-installer/locale string pt_BR.UTF-8
d-i keyboard-configuration/xkb-keymap select br
```

### Padronizar o domínio e o nome da máquina (hostname)

As definições de *hostname* e domínio feitas pelo DHCP têm precedência sobre as configurações abaixo, mas elas servem para que as perguntas desta etapa não sejam feitas.

```
d-i netcfg/get_hostname string debian
d-i netcfg/get_domain string localhome
```

Para forçar a customização de um *hostname*, nós utilizamos a linha abaixo:

```
d-i netcfg/hostname string debian
```

### Configurações do espelho

Para definir a suíte Debian a ser instalada (ex.: unstable):

```
d-i mirror/suite string unstable
```

Para definir a suíte dos componentes do instalador (ex.: unstable)

```
d-i mirror/udeb/suite string unstable
```

### Não pedir senha de root

Se quisermos padronizar a instalação e a configuração do `sudo` para o usuário administrativo, nós podemos saltar a etapa onde o instalador pede uma senha de `root`:

```
d-i passwd/root-login boolean false
```

### Definições de relógio

Predefine o relógio como UTC e a utilização de um servidor de relógio de rede:

```
d-i clock-setup/utc boolean true
d-i time/zone string America/Sao_Paulo
d-i clock-setup/ntp boolean true
```

### Define o estilo de montagem de dispositivos

```
d-i partman/mount_style select uuid
```

### Habilitar repositórios non-free e contrib

Habilita os repositórios `contrib` e `non-free` para permitir a instalação de pacotes dessas fontes pelo instalador. Estas opções **não habilitam** esses repositórios no sistema instalado, elas são utilizadas apenas pelo instalador.

```
d-i apt-setup/non-free boolean true
d-i apt-setup/contrib boolean true
```

### Desativar a etapa de seleção de espelhos

```
d-i apt-setup/use_mirror boolean false
```

### Desativar a pergunta sobre outras mídias de instalação

```
d-i apt-setup/cdrom/set-first boolean false
```

### Repositórios adicionais ('local' pode ser de 0 a 9)

Com estas opções, nós podemos habilitar outras fontes de pacotes para uso do instalador, inclusive repositórios de terceiros.

```
d-i apt-setup/local0/repository string http://deb.debian.org/debian unstable main contrib non-free
d-i apt-setup/local0/source boolean true
```

Para informar chaves de autenticação de repositórios adicionais:

```
d-i apt-setup/local0/key string http://servidor/caminho/chave
```

Opcionalmente, para que a autenticação da chave do repositório adicional seja **ignorada**, pode ser utilizada a configuração abaixo (insegura!):

```
d-i debian-installer/allow_unauthenticated boolean true
```

### Opções do tasksel

Neste exemplo, vamos predefinir apenas os utilitários padrão do sistema e o servidor SSH, mas é possível configurar a instalação de qualquer opção disponível no `tasksel`.

```
tasksel tasksel/first multiselect standard ssh-server
```

### Pacotes a serem incluídos na instalação (ex.: instalação i3wm)

Além dos grupos de pacotes do `tasksel`, com o preseed é possível instalar qualquer outro pacote disponível nas fontes selecionadas. No exemplo, faremos a instalação do gerenciador de janelas **i3wm** e de alguns outros programas.

```
d-i pkgsel/include string \
firmware-linux-nonfree firmware-atheros firmware-realtek git curl xorg \
i3-wm i3lock i3blocks suckless-tools arandr xfce4-terminal
```

### Pesquisa de popularidade

Nós podemos predefinir que queremos participar da pesquisa de popularidade de pacotes.

```
popularity-contest popularity-contest/participate boolean true
```

### Comando de pós instalação (uma única linha de comando!)

A configuração do `late_command` permite executar uma linha de comando no final da instalação, enquanto o diretório `target` ainda existe.

```
d-i preseed/late_command string \
echo "# Sources list gerada pelo preseed..." > /target/etc/apt/sources.list; \
echo "deb http://deb.debian.org/debian/ unstable main" >> /target/etc/apt/sources.list; \
echo "deb-src http://deb.debian.org/debian/ unstable main" >> /target/etc/apt/sources.list; \
in-target apt update
```

## 7 - Etapas de modificação da imagem de instalação

1. Verificar as dependências (`xorriso` e `syslinux-utils`)
2. Extrair seu conteúdo para uma pasta temporária (`xorriso`)
3. Detectar a arquitetura da imagem (amd64, i386, etc...)
4. Descompactar o arquivo `initrd.gz` (`gzip`)
5. Copiar o arquivo `preseed.cfg` para a raiz do `initrd` (`cpio`)
6. Compactar novamente o `initrd` (`gunzip`)
7. Editar e/ou incluir outros arquivos na imagem (opcional)
8. Recalcular o md5 da imagem (`md5sum`)
9. Gerar uma nova imagem com as alterações (`xorriso`)
10. Tornar a nova imagem bootavel (`isohybrid`)

## 8 - Modificando a imagem de instalação (netinstall)

### 8.1 - Verificar as dependências

Antes de começar, é preciso garantir que as dependências abaixo estejam satisfeitas:

| Dependências  | Pacotes            | Uso no procedimento                                             |
|---------------|--------------------|-----------------------------------------------------------------|
| `xorriso`     | `xorriso`          | Extrair e remasterizar a imagem do instalador.                  |
| `isohybrid`   | `syslinux-utils`   | Torna a imagem remasterizada bootável em CDs e dispositivos USB |
| `cpio`        | base do sistema    | Copia o `preseed.cfg` para o `initrd`                           |
| `gzip`        | base do sistema    | Recompacta o arquivo `initrd.gz`                                |
| `gunzip`      | base do sistema    | Extrai o conteúdo do arquivo `initrd.gz`                        |

### 8.2 - Extrair conteúdo da imagem

```
xorriso -osirrox on -indev ISO-ORIGINAL.ISO -extract / PASTA-DESTINO
```

### 8.3 - Detectar a arquitetura da imagem

Dependendo da imagem baixada, o caminho do `initrd.gz` será:

```
PASTA-DESTINO/install.ARQUITETURA/initrd.gz
```

### 8.4 - Descompactar o 'initrd.gz'

```
chmod -R +w "PASTA-DESTINO/install.ARQUITETURA"
gunzip "PASTA-DESTINO/install.ARQUITETURA/initrd.gz"
```

### 8.5 - Copiar o arquivo 'preseed.cfg' para o 'initrd'

```
echo preseed.cfg | cpio -H newc -o -A -F "PASTA-DESTINO/install.ARQUITETURA/initrd"
```

### 8.6 - Compactar novamente o 'initrd'

```
gzip "PASTA-DESTINO/install.ARQUITETURA/initrd"
chmod -R -w "PASTA-DESTINO/install.ARQUITETURA"
```

### 8.7 - Editar ou incluir outros arquivos no instalador

Opcionalmente, nós podemos aproveitar que a imagem do instalador está aberta para customizar outros componentes ou incluir pastas e arquivos que poderão ser utilizados nos comandos de pós instalação do preseed.

### 8.8 - Recalcular o md5 da imagem

Como o conteúdo da imagem foi alterado, é necessário recalcular a soma md5.

```
cd PASTA-DESTINO
md5sum $(find -follow -type f 2>/dev/null) > md5sum.txt
cd ..
```

### 8.9 - Remasterizar a imagem de instalação

```
xorriso -as mkisofs \
-c isolinux/boot.cat \
-b isolinux/isolinux.bin \
-no-emul-boot \
-boot-load-size 4 \
-boot-info-table \
-eltorito-alt-boot \
-e boot/grub/efi.img \
-no-emul-boot \
-isohybrid-gpt-basdat \
-o ISO-DESTINO.ISO \
PASTA-DESTINO/
```

### 8.10 - Tornar nova imagem bootável

```
isohybrid ISO-DESTINO.ISO
```

## Anexos

### Anexo 1: Meu exemplo de preseed.cfg

```
d-i netcfg/get_hostname string debian
d-i netcfg/get_domain string localhome

d-i netcfg/hostname string pstest

d-i mirror/suite string unstable
d-i mirror/udeb/suite string unstable

d-i passwd/root-login boolean false

d-i clock-setup/utc boolean true
d-i clock-setup/ntp boolean true

d-i partman/mount_style select uuid

d-i apt-setup/non-free boolean true
d-i apt-setup/contrib boolean true

d-i apt-setup/use_mirror boolean false

d-i apt-setup/cdrom/set-first boolean false

d-i apt-setup/local0/repository string http://deb.debian.org/debian unstable main contrib non-free
d-i apt-setup/local0/source boolean true

tasksel tasksel/first multiselect standard ssh-server

d-i pkgsel/include string \
firmware-linux-nonfree firmware-atheros firmware-realtek git curl xorg \
i3-wm i3lock i3blocks suckless-tools arandr xfce4-terminal

popularity-contest popularity-contest/participate boolean true

d-i preseed/late_command string \
echo "# Sources list gerada pelo preseed..." > /target/etc/apt/sources.list; \
echo "deb http://deb.debian.org/debian/ unstable main" >> /target/etc/apt/sources.list; \
echo "deb-src http://deb.debian.org/debian/ unstable main" >> /target/etc/apt/sources.list; \
in-target apt update
```

### Anexo 2: Script de inserção do 'preseed.cfg' na imagem de instalação

```
#!/usr/bin/env bash

# Uso:
#
# ./custom-iso imagem-original.iso imagem-nova.iso caminho/arquivo/preseed.cfg


# 1. Caminho de extração da imagem original (tmp_dir)

tmp_dir="mnt"

# 2. Verificar se usuário é root

if [[ $EUID -ne 0 ]]; then
  echo -e "\nUse 'sudo' para executar este script!\n" 1>&2
  exit 1
fi

# 3. Dependências

declare -A deps

deps[isohybrid]=syslinux-utils
deps[xorriso]=xorriso

for d in "${!deps[@]}"; do
  [[ -z $(command -v $d) ]] \
    && echo -e "\n\n-- ${deps[$d]} não instalado, saindo...\n\n" \
    && exit 1
done

# 4. Extrair imagem original

echo -e "\n-- Extraindo imagem para $tmp_dir..."
xorriso -osirrox on -indev $1 -extract / "$tmp_dir" &> /dev/null

# 5. Detectar a arquitetura da imagem

arch=$(find $tmp_dir/install.* -maxdepth 0 -type d)
arch=${arch##*.}
echo "-- Arquitetura detectada: $arch"

# 6. Descompactar initrd.gz

chmod -R +w "$tmp_dir/install.$arch"
echo "-- Extraindo $tmp_dir/install.$arch/initrd.gz..."
gunzip "$tmp_dir/install.$arch/initrd.gz"

# 7. Incluir o preseed em initrd

echo -e "-- Incluindo arquivo $3 em initrd... \c"
[[ "$3" != preseed.cfg ]] && cp $3 preseed.cfg
echo preseed.cfg | cpio -H newc -o -A -F "$tmp_dir/install.$arch/initrd"
[[ "$3" != preseed.cfg ]] && rm preseed.cfg

# 8. Recomprimir o initrd

echo "-- Comprimindo o initrd..."
gzip "$tmp_dir/install.$arch/initrd"
chmod -R -w "$tmp_dir/install.$arch"

# 9. Atualizar a soma md5

echo "-- Atualizando md5sum.txt..."
cd $tmp_dir
md5sum $(find -follow -type f 2>/dev/null) > md5sum.txt
cd ..

# 10. Gerar a nova imagem

echo -e "-- Gerando imagem $2..."

xorriso -as mkisofs \
-c isolinux/boot.cat \
-b isolinux/isolinux.bin \
-no-emul-boot \
-boot-load-size 4 \
-boot-info-table \
-eltorito-alt-boot \
-e boot/grub/efi.img \
-no-emul-boot \
-isohybrid-gpt-basdat \
-o $2 "$tmp_dir/"

real_usr="$(logname)"
chown $real_usr:$real_usr $2

# 11. Tornar nova imagem bootável por por USB

echo -e "-- Tornando $2 bootable..."
isohybrid $2

# 12. Limpeza

rm -fr $tmp_dir
echo -e "\nPronto!\n"

exit 0

```