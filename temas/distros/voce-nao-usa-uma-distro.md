# Você não usa uma distribuição

Uma das coisas mais interessantes entre os usuários de sistemas operacionais com base no GNU/Linux (ou apenas Linux, em alguns casos) é o nosso hábito de dizer que usamos a distribuição X ou Y. O fato é que, tirando alguns momentos bem específicos, isso dificilmente será verdade.

## O que seria uma distribuição?

A rigor, uma distribuição é um conjunto de ferramentas que nos dão acesso ao software que queremos utilizar (repositórios). As distribuições nos fornecem um kernel, um shell, utilitários para gerenciar vários aspectos do sistema, ambientes gráficos, programas para finalidades diversas e, na maioria dos casos, ferramentas para instalar e administrar todo esse mundo de componentes computacionais.

Nesse contexto, toda distribuição se resume a:

* Um instalador
* Um sistema de empacotamento
* Um repositório
* Gerenciadores de pacotes
* Uma infraestrutura para manter tudo isso

Suporte e prestações de serviços diversos também poderiam constar dessa lista, mas seriam adicionais que poderiam ou não estar ligados diretamente ao conceito de distribuição. Também é um fato que as distribuições podem formar **em torno de si** comunidades de usuários, desenvolvedores e colaboradores, mas isso é algo periférico que não responde a questão sobre o quê, de fato, estamos usando. 

## Políticas e características

A maioria das pessoas chega a uma distribuição por indicação de alguém de confiança ou, mais comum ainda, porque ela já foi instalada em suas máquinas. Existe um outro contingente, porém, que observa outros fatores, como os aspectos técnicos, o conjunto de pacotes que é oferecido, as políticas de empacotamento e licenciamento, etc.

Por exemplo, segundo esses parâmetros, a minha escolha foi pelo Debian, justamente porque ele me oferece políticas e características que me agradam mais. É por isso que essa é a distribuição que coloca as coisas que eu uso na minha máquina. Depois que colocou, eu não uso mais o Debian, eu uso os programas e tenho uma ferramenta que a distribuição me oferece para gerenciar o que eu tenho instalado e até instalar mais programas na minha máquina. Mas, do ponto de vista da minha computação, acaba aí o meu uso do Debian.

Aliás, eu posso ter afinidade com o projeto que mantém uma distribuição funcionando, posso chegar ao ponto de me envolver de alguma forma com esse projeto, mas isso estaria mais para ajudar a manter a distribuição do que para "usar". 

## Quando eu realmente uso uma distribuição?

Para ser exato, nunca. Você pode utilizar o instalador, as ferramentas e os repositórios de uma distribuição, pode utilizar os serviços que ela oferece como um negócio ou através de uma comunidade voluntária, mas nada disso é "usar" uma distribuição.

Simbolicamente, talvez, você pode até dizer que usa a distribuição quando se serve dos repositórios e da infraestrutura que ela oferece, mas isso acontece em ocasiões muito pontuais e, em todo o restante do tempo, você está usando um kernel, um shell, um navegador, um editor de textos, um cliente de e-mail, mas nada disso é a distribuição.

## Eu não posso mudar de onde eu não estou

Na minha cabeça sempre funcionou assim: eu trabalho, eu programo, eu escrevo, eu estudo, eu converso, eu me divirto utilizando os programas que estão no meu computador. Todos esses programas estão disponíveis para mim independente da distribuição que eu escolhi para ter acesso a eles. Então, "mudar" de distribuição significa apenas mudar o fornecedor das coisas que eu uso. É essa característica que faz com que "usar" e "mudar" tenham muito pouco sentido quando falamos de GNU/Linux.

Se fosse algo monolítico, algo no modelo "porteira fechada", como acontece nos sistemas operacionais proprietários, eu poderia dizer que sim, eu uso, eu mudo, etc... Mas as distribuições saem da nossa frentre no momento em que terminamos a instalação, e deve ser assim!

## Pare de usar distribuições e seja feliz e produtivo!

Se você observar, boa parte das pessoas vivem "mudando" de distribuições, os chamamos "distro hoppers", está nesta ciranda sem fim justamente porque, em vez de usar os programas, está sempre tentando usar a distribuição. Para elas, é como se algo estivesse faltando, algo que talvez a próxima distribuição possa oferecer. Mas, infelizmente, elas ainda não entenderam que estão apenas mudando de fornecedores das mesmas coisas. 

É fato: as versões dos programas, a quantidade de pacotes, as otimizações do kernel, a permissividade com o software fornecido, tudo isso pode variar de uma distribuição para outra! Isso pode, sim, interferir na sua computação e na sua produtividade. Por isso mesmo, testar e avaliar aspectos técnicos é algo bem diferente de adiar o que você realmente precisa fazer com a desculpa de encontrar a "distro perfeita".

## "Blau, não seja chato!"

Sim, eu sou chato com essas coisas, mas eu também acredito que alguém precisa nos lembrar disso de vez em quando. Pode ver, a maioria das brigas intermináveis nas redes sociais acontece justamente porque as pessoas ficam olhando para o fornecedor e se esquecem (ou se esquivam) de olhar para o que é fornecido. Discussões sobre "qual é a melhor distro", "qual é o melhor ambiente gráfico", nunca fazem sentido! Talvez façam se vierem seguidas de um "para tal coisa", desde que essa "tal coisa" não seja algo que todas as distribuições oferecem.

Portanto, diga com toda tranquilidade que é usuário da distribuição que você escolheu, como quem diz que é usuário de um serviço, mas não se esqueça de ser usuário das ferramentas que você escolheu para fazer o seu trabalho e tornar a sua vida mais fácil. E dificilmente, em raros casos, essa ferramenta será uma distribuição.

