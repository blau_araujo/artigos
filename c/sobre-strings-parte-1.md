# Sobre strings na linguagem C

## Não existe o tipo 'string'

A linguagem C carece de abstrações para o trabalho com strings e, especialmente, para o trabalho com caracteres Unicode. Uma string, por definição na linguagem, é um vetor de caracteres cujo último elemento é o caractere nulo (`\0`).

Por sua vez, o tipo caractere (`char`) se refere a um valor inteiro com o tamanho de 1 byte, e esse é o ponto da tipagem na linguagem C: **a previsibilidade do endereçamento e do espaço ocupado por um dado na memória**.

Portanto, dados que frequentemente podem ocupar espaços e endereços variáveis na memória, como arrays e strings (arrays de caracteres), vão exigir um tratamento especial e, consequentemente, nos darão mais trabalho na linguagem C do que nas linguagens que criaram abstrações para lidar com isso.

## Arrays e ponteiros

O conceito de `array` está diretamente ligado ao conceito de `ponteiros`. A começar pelo fato de que **o nome de uma array é um ponteiro para o endereço do seu primeiro elemento** na memória. Isso deixa bastante evidente que, se queremos trabalhar com arrays e strings, nós precisamos entender primeiro o que são ponteiros e como nós trabalhamos com eles.

## Ponteiros

O **ponteiro** é um tipo especial de variável que, em vez de armazenar valores, armazena endereços de dados na memória. Por exemplo, quando nós definimos uma variável como...

```
char caractere;
```

A variável `caractere` irá armazenar um dado do tipo `char` que, por sua vez, ocupará sempre 1 byte na memória e estará sempre localizado em um certo endereço alocado pelo compilador. 

Para vermos a localização do valor em `caractere` nós utilizamos o operador unário de endereço: `&`. Observe o exemplo abaixo:

```
#include <stdio.h>

int main() {
    
    char caractere = 'A';
    
    printf(
        "Endereço de 'caractere'     : %u\n"
        "Valor de 'caractere'        : %c\n"
        "Valor inteiro de 'caractere': %d\n",
        &caractere,
        caractere,
        caractere
    );
}
```

O resultado na saída seria algo como:

```
Endereço de 'caractere'     : 857689711
Valor de 'caractere'        : A
Valor inteiro de 'caractere': 65
```

> **Importante:** O operador de endereço só pode ser utilizado com variáveis!

Voltando aos ponteiros, um ponteiro é uma **variável** que armazena o endereço de um dado na memória e, portanto, ele mesmo é uma variável e precisa obrigatoriamente ser declarado conforme seu tipo. 

*Mas, qual é o tipo de um ponteiro?*

O tipo associado a um ponteiro será o tipo do dado para o qual ele aponta. Se o dado for um inteiro, nós declaramos que o ponteiro é do tipo `int`, se for um caractere, será `char`, e assim por diante.

*Mas você disse que ponteiros armazenam endereços, e endereços possuem o mesmo tamanho de inteiros sem sinal (`unsigned int`)!*

Exatamente! É por isso que o tipo associado a um ponteiro se chama **tipo base**, e a declaração do seu tipo não serve para reservar um espaço para ele na memória, já que todo ponteiro, independente do tipo base, sempre terá o mesmo tamanho em bytes.

Sendo assim, quando nós declaramos o tipo base de um ponteiro, nós estamos apenas determinando o tamanho que o dado em si (não o ponteiro) terá a partir de seu endereço base na memória.

*E como nós declaramos um ponteiro?*

Como o ponteiro não aponta exatamente para o dado, e sim para seu endereço, nós utilizamos o operador unário de indireção: `*`, ou seja, se um ponteiro aponta para o endereço de um dado, o valor do dado pode ser obtido indiretamente a partir de seu endereço, daí o termo **indireção**.

A sintaxe geral da declaração de um ponteiro é:

```
tipo_base *nome_do_ponteiro;
```

Por exemplo:

```
int *pint;   // Ponteiro para um inteiro
char *pchr;  // Ponteiro para um caractere
float *pflt; // Ponteiro para um número de ponto flutuante
```

Vejamos isso na prática:

```
#include <stdio.h>

int main() {

    char caractere = 'A';
    char *pchr;
    
    pchr = &caractere;

    printf(
        "Antes da mudança\n\n"
        "Valor em 'pchr': %u\n"
        "Valor em '*pchr'   : %c\n\n",
        pchr,
        *pchr
    );
    
    *pchr = 'B';
    
    printf(
        "Depois da mudança\n\n"
        "Endereço de 'caractere': %u\n"
        "Valor em 'caractere'   : %c\n",
        &caractere,
        caractere
    );    
}
```

A saída deve ser algo assim:

```
Antes da mudança

Valor em 'pchr': 4205132287
Valor em '*pchr'   : A

Depois da mudança

Endereço de 'caractere': 4205132287
Valor em 'caractere'   : B

```

Na primeira parte, nós declaramos a variável `caractere` e o ponteiro `pchr`:

```
char caractere = 'A';
char *pchr;
```

Em seguida, nós atribuímos o endereço de `caractere` a `pchr`:

```
pchr = &caractere;
```

Dessa forma, o valor em `pchr` é o endereço de `caractere` e o valor em `caractere` pode ser acessado indiretamente a partir do endereço em `pchr` utilizando o operador de indireção:

```
printf("%c", *pchr); // retornando o valor no endereço
*pchr = 'B';         // atribuindo um novo valor no endereço
```

Um ponteiro também pode ser iniciado com a atribuição de um endereço:

```
char c = 'A';
char *pchr = &c;

printf("%u\n", pchr);  // retorna o endereço de 'c'
printf("%c\n", *pchr); // retorna o valor em 'c'
```

## Aritmética de ponteiros

A primeira coisa que precisamos entender sobre a aritmética de ponteiros é que ela é bem diferente da aritmética a que estamos acostumados. Para começar, nós só podemos fazer três tipos de operações aritméticas com os ponteiros:

* Soma de um inteiro a um ponteiro
* Subtração de um inteiro de um ponteiro
* Subtração de dois ponteiros do mesmo tipo

A segunda coisa importante a ter em mente é que, como estamos trabalhando com endereços base associados a tipos de dados, somar `1` significa apontar para o endereço seguinte considerando o tamanho em bytes do tipo de dado, mesmo que este novo endereço não seja uma localização válida da memória (e geralmente não será, no caso dos tipos primitivos).  

Vejamos alguns exemplos:

```
char c = 'A'   , *pc = &c;
int i = 10     , *pi = &i;
double d = 1.5 , *pd = &d;

printf("pc = %u; pc + 1 = %u\n", pc, pc + 1);
printf("pi = %u; pi + 1 = %u\n", pi, pi + 1);
printf("pd = %u; pd + 1 = %u\n", pd, pd + 1);
```

Uma saída típica seria:

```
pc = 2063783631; pc + 1 = 2063783632
pi = 2063783612; pi + 1 = 2063783616
pd = 2063783592; pd + 1 = 2063783600
```

Observe que:

* `pc + 1` deu um salto de 1 byte na memória
* `pi + 1` deu um salto de 4 bytes na memória
* `pd + 1` deu um salto de 8 bytes na memória

O que existe nesses novos endereços é totalmente indeterminado, mas é assim que as operações aritméticas funcionam com os ponteiros.

Já a subtração entre ponteiros do mesmo tipo resultará no tamanho em bytes do tipo do dado levando em conta quem aponta para o endereço mais alto, por exemplo:

```
int i1 = 10, *pi1 = &i1;
int i2 = 10, *pi2 = &i2;

printf("pi1 = %u\n", pi1);
printf("pi2 = %u\n", pi2);
printf("pi1 - pi2 = %d\n", pi1 - pi2);
printf("pi2 - pi1 = %d\n", pi2 - pi1);
```

Como o endereço base de `pi1` é mais alto do que o de `pi2`, isso produziria na saída:

```
pi1 = 777892172
pi2 = 777892156
pi1 - pi2 = 4
pi2 - pi1 = -4
```

## Ponteiros para ponteiros

Além de armazenar um endereço, o ponteiro é uma variável e tem o seu próprio endereço na memória. Portanto, nós também podemos armazenar os endereços de um ponteiro em outros ponteiros. Neste caso, a sintaxe geral da declaração de um ponteiro para um ponteiro seria:

```
tipo_base **nome_do_ponteiro
```

Vejamos o exemplo abaixo:

```
char c = 'A';
char *pc = &c;
char **ppc = &pc;
```

Como vimos, `*pc` representa o valor no endereço em `pc` que, por sua vez, é o valor em `c` (`A`). Mas, qual seria o valor em `**ppc`?

Lembrando que cada `*` é um operador unário de indireção, `**ppc` também poderia ser escrito como `*(*ppc)`, onde `*ppc` é o valor armazenado em `ppc`, ou seja, o endereço armazenado em `pc`. Quando aplicamos a segunda indireção, nós temos o valor armazenado nesse endereço, que é o valor da variável `c`.

```
**ppc == *(*ppc)

(*ppc) == endereço em 'pc' => endereço de 'c'

*(endereço em 'pc') == valor no endereço de 'c'
```

Portanto, `**ppc` representa o valor armazenado em `c`.

Isso é particularmente interessante quando vemos as duas notações possíveis para a array de argumentos (`argv`) passados pela linha de comando para a função `main()`:

```
int main(int argc, char *argv[]) ...
```

Se o nome de uma array é um ponteiro para o endereço de seu primeiro elemento, então, estamos diante de um ponteiro para um ponteiro, e nós poderíamos declarar os parâmetros formais da função `main()` assim:

```
int main(int argc, char **argv) ...
```

## Ponteiros e arrays unidimensionais (vetores)

Em uma array unidimensional (também chamada de *vetor*), os dados de cada elemento são armazenados em espaços contíguos da memória levando em conta seu tamanho em bytes. Por exemplo, na array abaixo:

```
int arr[5] = {1, 2, 3, 4, 5};
```

Os valores nos elementos seriam dispostos da seguinte forma na memória:

```
&arr[0] => endereço X
&arr[1] => endereço X + 4
&arr[2] => endereço X + 8
&arr[3] => endereço X + 12
&arr[4] => endereço X + 16
```

Como dissemos, logo no início deste artigo, **o nome de uma array é um ponteiro para o endereço do seu primeiro elemento**. Agora podemos dizer que a coisa vai um pouco mais fundo: o nome de uma array é uma **constante**, ou seja, é a representação literal de um ponteiro.

Consequentemente, nós temos duas formas de representar os valores em uma array:

* Através dos índices (subscritos), ou...
* Através de ponteiros!

Aliás, por baixo do capô, o compilador sempre acessa os valores de uma array através de ponteiros. Mais do que uma questão de eficiência, isso acontece porque a notação com índices é apenas uma abstração daquilo que as arrays realmente são: ponteiros.

Ainda no exemplo acima, `&arr[0]` e `arr` representam exatamente o mesmo endereço, assim como `&arr[3]` e `arr + 3`, pela aritmética de ponteiros, também apontam para o mesmo endereço...

```
&arr[0] == arr     => endereço X
&arr[1] == arr + 1 => endereço X + 4
&arr[2] == arr + 2 => endereço X + 8
&arr[3] == arr + 3 => endereço X + 12
&arr[4] == arr + 4 => endereço X + 16
```

Portanto, de forma equivalente...

```
arr[0] == *arr       => valor no índice 0
arr[1] == *(arr + 1) => valor no índice 1
arr[2] == *(arr + 2) => valor no índice 2
arr[3] == *(arr + 3) => valor no índice 3
arr[4] == *(arr + 4) => valor no índice 4
```

Mas existe uma diferença: o nome da array é um **ponteiro constante** (literal), não uma variável do tipo ponteiro. Sendo assim, não podemos atribuir novos endereços a nomes de arrays, por exemplo:

```
arr++;         // Errado!
arr = arr + 3; // Errado!
arr = &var;    // Errado!
```

Contudo, isso é possível:

```
#include <stdio.h>

int main() {

    int arr[5] = {1, 2, 3, 4, 5};
    int *pa = arr; // É o mesmo que: int *pa = &arr[0];
    int i;

    for (i = 0; i < 5; i++) {
                
        printf(
            "Valor em arr[%d] = %d - "
            "Endereço de arr[%d] = %u\n",
            i, *pa, i, pa
        );
     
        // Alterando o endereço em 'pa'
        pa++;
    }

    return 0;
}
```

A saída seria algo assim:

```
Valor em arr[0] = 1 - Endereço de arr[0] = 1499866220
Valor em arr[1] = 2 - Endereço de arr[1] = 1499866224
Valor em arr[2] = 3 - Endereço de arr[2] = 1499866228
Valor em arr[3] = 4 - Endereço de arr[3] = 1499866232
Valor em arr[4] = 5 - Endereço de arr[4] = 1499866236
```

## O que tudo isso tem a ver com strings?

Como já dissemos, na linguagem C, as strings são arrays de caracteres contendo o caractere nulo (`\0`) no último elemento -- se uma array de caracteres não for terminada com o caractere nulo, ela é só isso, uma array de caracteres, e não uma string.

Ou seja, isso é uma string:

```
char fruta[7] = {'b', 'a', 'n', 'a', 'n', 'a', '\0'};
```

Mas strings também podem ser representadas como constantes (literais):

```
char fruta[] = "banana";
```

Neste caso, o compilador vai "enxergar" as aspas duplas, vai saber que se trata de uma string literal e ele mesmo vai adicionar o caractere nulo ao final. Então, a string literal `"banana"` será armazenada da seguinte forma na memória:

```
b  => endereço X
a  => endereço X + 1
n  => endereço X + 2
a  => endereço X + 3
n  => endereço X + 4
a  => endereço X + 5
\0 => endereço X + 6
```

> **Nota:** até uma string literal vazia (`""`) contém o caractere nulo!

Se strings são arrays, as strings literais são tratadas da mesma forma que as arrays, ou seja, uma string literal é um ponteiro para o endereço do primeiro elemento de uma array do tipo `char`.

Veja que interessante:

```
#include <stdio.h>

int main() {

    for (int i = 0; i < 6; i++) {
                
        printf(
            "Caractere %d de \"banana\" = %c - "
            "Endereço = %u\n",
            i, *("banana" + i), "banana" + i
        );
     
    }

    return 0;
}
```

Esta seria a saída...

```
Caractere 0 de "banana" = b - Endereço = 1455682359
Caractere 1 de "banana" = a - Endereço = 1455682360
Caractere 2 de "banana" = n - Endereço = 1455682361
Caractere 3 de "banana" = a - Endereço = 1455682362
Caractere 4 de "banana" = n - Endereço = 1455682363
Caractere 5 de "banana" = a - Endereço = 1455682364
```

Portanto, nós podemos atribuir uma string literal a um ponteiro de tipo base `char`:

```
char *fruta = "banana";
```

Isso nos permite aplicar tudo que vimos sobre arrays e ponteiros até aqui:

```
#include <stdio.h>

int main() {

    char *fruta = "banana";
    /*
    Utilizando...
    
    char fruta[] = "banana";
    
    Este exemplo não seria possível, já que
    o nome de uma array é um ponteiro constante!
    */

    for (int i = 0; i < 6; i++) {
                
        printf(
            "Valor em fruta[%d] = %c - "
            "Endereço de fruta[%d] = %u\n",
            i, *fruta, i, fruta
        );
     
        /*
        Atribuindo a string literal a um ponteiro,
        é possível alterar o endereço em 'fruta'
        */
        fruta++;
    }

    return 0;
}
```

O que gera a saída...

```
Valor em fruta[0] = b - Endereço de fruta[0] = 2660427328
Valor em fruta[1] = a - Endereço de fruta[1] = 2660427329
Valor em fruta[2] = n - Endereço de fruta[2] = 2660427330
Valor em fruta[3] = a - Endereço de fruta[3] = 2660427331
Valor em fruta[4] = n - Endereço de fruta[4] = 2660427332
Valor em fruta[5] = a - Endereço de fruta[5] = 2660427333
```

Ainda sobre as observações deste exemplo, quando inicializamos uma string na forma de uma array de caracteres, a inicialização só pode ser feita com uma string literal ou com caracteres agrupados entre chaves. Ainda é possível utilizar a aritmética de ponteiros com a string literal, mas o valor retornado seria um caractere, não uma string literal. Portanto...

```
char fruta[] = *("banana" + 2); // Isso provocaria um erro!
```

Mas isso funcionaria:

```
#include <stdio.h>

int main() {

    char fruta[] = {*("banana" + 2),'\0'};

    for (int i = 0; i < 2; i++) {
                
        printf("Valor em fruta[%d] = %c\n", i, fruta[i]);
     
    }

    return 0;
}
```

Retornando...

```
Valor em fruta[0] = n
Valor em fruta[1] = 
```

Outra coisa importante a se observar é que, mesmo que o tamanho da array não tenha sido declarado explicitamente no exemplo acima, isso não significa que a quantidade de elementos (caracteres, no caso) possa ser aumentado livremente. Assim que a string foi inicializada, ela passou a ter 2 elementos, e nós não podemos mudar isso sem envolver alguma manipulação da memória.

Mas esse é só o começo dos problemas...

## O problema da atribuição

Arrays (e, consequentemente, strings) não suportam a operação de atribuição após terem sido declaradas!

```
// Isso está ok...
char str[] = "banana";

// Isso também está ok...
char *str;
str = "banana";

// Mas, isso provoca um erro...
char str[7];
str = "banana";

// Assim como isso...
char str[];
str = "banana";
```

Porém, tenha em mente que estamos falando de arrays e strings como um todo. Individualmente, cada elemento de uma array pode receber um valor por atribuição a qualquer momento, e o mesmo vale para os caracteres de uma string. Então, não há problema algum em apenas declarar uma array sem inicializá-la -- basta saber como atribuir valores a ela.

## Copiando strings literais para variáveis

Observe o exemplo abaixo:

```
#include <stdio.h>

int main() {
    
    char str[7];

    for (int i = 0; i < 6; i++) {
        str[i] = 'a' + i;
    }
    
    str[6] = '\0';
    
    printf("%s\n", str);
    return 0;
}
```

Aqui, nós declaramos a string `str` com 7 elementos mas não a inicializamos.

```
char str[7];
```

Até este ponto, os valores nos seus elementos são literalmente "lixo" (*garbage*, conteúdos anteriores na memória). Como não podemos atribuir uma string literal à variável `str` como um todo, vamos fazer isso atribuindo caracteres a cada um dos seus 7 elementos (lembrando que o último deve ser `\0`, o caractere nulo):

```
for (int i = 0; i < 6; i++) {
    str[i] = 'a' + i;
}

str[6] = '\0';
```

> **Nota:** Caracteres (como o `'a'` do exemplo) são tratados como **inteiros de 1 byte** na linguagem C e, portanto, podem participar de operações aritméticas normalmente. Neste caso, eles irão representar seus respectivos valores decimais na tabela ASCII.

Como resultado, nós teríamos a string:

```
abcdef
```

O mesmo princípio pode ser aplicado à cópia de strings literais:

```
#include <stdio.h>

int main() {
    
    char str[7];

    for (int i = 0; i < 6; i++) {
        str[i] = *("banana" + i);
    }
    
    str[6] = '\0';
    
    printf("%s\n", str);
    return 0;
}
```

Neste exemplo, nós utilizamos a aritmética de ponteiros para atribuir cada caractere da string literal `"banana"` a um elemento de `str`:

```
for (int i = 0; i < 6; i++) {
    str[i] = *("banana" + i);
}
str[6] = '\0';
```

Aplicando a mesma ideia de uma forma mais elaborada (e **ilustrativa**), nós poderíamos fazer algo como o programa abaixo:

```
#include <stdio.h>

#define MAX 10

int main() {
    
    char destino[MAX];
    char origem[] = "banana";
    size_t osize = sizeof(origem) - 1;
    
    printf("String origem: %s\n", origem);
    printf("Tamanho      : %ld\n\n", osize);
    
    for (int i = 0; i < MAX; i++) {
        if (i >= osize || i == (MAX - 1)) {
            destino[i] = '\0';
            if (destino[i] == '\0') printf("(nulo) ", destino[i]);
        } else{
            destino[i] = origem[i];
            printf("(%c) ", destino[i]);
        }
    }
    
    printf("\n\nString destino: %s\n", destino);
    
    return 0;
}
```

Que produziria na saída...

```
String origem: banana
Tamanho      : 6

(b) (a) (n) (a) (n) (a) (nulo) (nulo) (nulo) (nulo) 

String destino: banana
```

Vamos tentar compreender o que está acontecendo por partes.

```
char destino[MAX];
char origem[] = "banana";
```

A string `destino` terá seu tamanho máximo (incluindo o caractere nulo) determinado pela constante `MAX`, definida com o valor `10` no exemplo. Isso significa que ela pode receber apenas 10 caracteres, ou teremos um *estouro de buffer*, que é o que acontece quando dados são escritos em endereços de memória adjacentes aos limites determinados para uma variável. Geralmente, o programa causaria apenas uma falha de segmentação, mas o comportamento, na verdade, é indeterminado. 

Por exemplo, se em vez de `"banana"` a string `origem` fosse bem maior, e o nosso código não tivesse a condição abaixo, onde o valor de `i` é comparado com `MAX - 1`...


```
if (i >= osize) {
    destino[i] = '\0';
    ...
} 
```

Veja o que aconteceria:

```
...
char origem[] = "testando os limites da string";
...
```

Saída:

```
String origem: testando os limites da string
Tamanho      : 29

(t) (e) (s) (t) (a) (n) (d) (o) ( ) (o) 

String destino: testando o��U
```

> **Nota:** Mesmo que não aconteça nenhum erro visível, esse tipo de problema representa uma vulnerabilidade para o sistema que pode ser explorado em vários tipos de ataques.

Isso pode ser especialmente desastroso se não soubermos que strings em Unicode podem ser bem maiores (em bytes) do que parecem. Veja o que acontece quando trocamos `"banana"` por `iniciação`, uma string com 9 caracteres:

```
String origem: iniciação
Tamanho      : 11

(i) (n) (i) (c) (i) (a) (�) (�) (�) (�) 

String destino: iniciaçã5~�gU
```

A segunda coisa importante a notar no exemplo, é que nós estamos calculando o tamanho da string `origem`. O objetivo é acompanhar quando ela termina para, a partir daí, preencher os elementos restantes da string `destino` com caracteres nulos.


```
size_t osize = sizeof(origem) - 1;

...

if (i >= osize || i == (MAX - 1)) {
    destino[i] = '\0';
    ...
}
```

Com isso, nossa ideia é garantir que, caso a string `origem` seja muito pequena, a string `destino` tenha uma terminação adequada e que os caracteres restantes não sejam preenchidos com o "lixo" pré-existente na memória.

Isso é muito semelhante ao que acontece com a função `strncpy()`, do cabeçalho `string.h`, com uma vantagem: quando a string de origem é maior do que o tamanho da string de destino, `strncpy()` não garante o caractere nulo na terminação!

Por outro lado, nosso código tem o mesmo problema de `strncpy()`: se a string de origem for muito curta, tanto o programa quanto a função gastarão muitos ciclos preenchendo o restante da string de destino com caracteres nulos.

Um último problema do nosso código em relação à função `strncpy()` é que ela consegue lidar muito bem com caracteres Unicode, e isso ainda é algo que teremos que resolver.


